v {xschem version=3.4.4 file_version=1.2
}
G {}
K {}
V {}
S {}
E {}
N 160 -220 160 -190 {
lab=D}
N 90 -160 120 -160 {
lab=EN}
N 160 -130 160 -100 {
lab=S}
N 160 -160 230 -160 {
lab=VSS}
N 350 -140 350 -110 {
lab=VSS}
N 280 -80 310 -80 {
lab=VSS}
N 350 -50 350 -20 {
lab=VSS}
N 350 -80 420 -80 {
lab=VSS}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 140 -160 0 0 {name=XN0
w=4
l=18n
nf=2
model=nmos4_lvt
spiceprefix=X
}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 330 -80 0 0 {name=XDUM
w=4
l=18n
nf=4
model=nmos4_lvt
spiceprefix=X
}
C {devices/iopin.sym} 20 -290 0 0 {name=p1 lab=VDD}
C {devices/iopin.sym} 20 -270 0 0 {name=p2 lab=VSS}
C {devices/iopin.sym} 160 -220 3 0 {name=p3 lab=D}
C {devices/iopin.sym} 160 -100 1 0 {name=p4 lab=S}
C {devices/iopin.sym} 90 -160 2 0 {name=p5 lab=EN}
C {devices/lab_pin.sym} 230 -160 2 0 {name=l1 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 350 -140 2 0 {name=l2 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 420 -80 2 0 {name=l3 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 350 -20 2 0 {name=l4 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 280 -80 0 0 {name=l5 sig_type=std_logic lab=VSS}
