from typing import *
from abs_templates_ec.analog_core import AnalogBase
from bag.layout.routing import TrackManager, TrackID

from sal.row import *
from sal.transistor import *

from .params import nmos_switch_layout_params


class layout(AnalogBase):
    """
    Parameters
    ----------
    temp_db : :class:`bag.layout.template.TemplateDB`
           the template database.
    lib_name : str
       the layout library name.
    params : dict[str, any]
       the parameter values.
    used_names : set[str]
       a set of already used cell names.
    **kwargs :
       dictionary of optional parameters.  See documentation of
       :class:`bag.layout.template.TemplateBase` for details.
    """

    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
        self._sch_params = None
    
    @classmethod
    def get_params_info(cls) -> Dict[str, str]:
        """
        Returns a dictionary containing parameter descriptions.

        Override this method to return a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : dict[str, str]
           dictionary from parameter name to description.
        """
        return dict(
            params='nmos_switch_layout_params parameter object',
        )

    def draw_layout(self):
        params: nmos_switch_layout_params = self.params['params']

        # horiz_conn_layer = self.mos_conn_layer + 1
        vert_conn_layer = self.mos_conn_layer + 2

        # 1:   Set up track allocations for each row
        tr_manager = TrackManager(self.grid, params.tr_widths, params.tr_spaces)

        # Set up the row information
        row_Dummy_NB = Row(name='Dummy_NB',
                           orientation=RowOrientation.R0,
                           channel_type=ChannelType.N,
                           width=params.w_dict['Dummy_NB'],
                           threshold=params.th_dict['Dummy_NB'],
                           wire_names_g=['sig1'],
                           wire_names_ds=['I', 'O']
                           )

        row_nmos = Row(name='n',
                       orientation=RowOrientation.R0,
                       channel_type=ChannelType.N,
                       width=params.w_dict['n'],
                       threshold=params.th_dict['n'],
                       wire_names_g=['IN'],
                       wire_names_ds=['I', 'O']
                       )

        row_Dummy_NT = Row(name='Dummy_NT',
                           orientation=RowOrientation.R0,
                           channel_type=ChannelType.N,
                           width=params.w_dict['Dummy_NT'],
                           threshold=params.th_dict['Dummy_NT'],
                           wire_names_g=['sig1'],
                           wire_names_ds=['I', 'O']
                           )

        # Define the order of the rows (bottom to top) for this analogBase cell
        rows = RowList([row_Dummy_NB, row_nmos, row_Dummy_NT])

        # Initialize the transistors in the design
        divide = 1
        fg_n = params.seg_dict['n'] // divide

        nmos = Transistor(name='n', row=row_nmos, fg=fg_n, seff_net='S', deff_net='D')

        # Compose a list of all the transistors so it can be iterated over later
        transistors = [nmos]

        # 3:   Calculate transistor locations
        fg_single = nmos.fg
        fg_total = fg_single + 2 * params.ndum
        fg_dum = params.ndum

        # Calculate positions of transistors
        nmos.assign_column(offset=fg_dum, fg_col=fg_single, align=TransistorAlignment.CENTER)

        # 4:  Assign the transistor directions (s/d up vs down)
        nmos.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)

        n_rows = rows.n_rows
        p_rows = rows.p_rows

        # 5:  Draw the transistor rows, and the transistors
        # Draw the transistor row bases
        self.draw_base(params.lch, fg_total, params.ptap_w, params.ntap_w,
                       n_rows.attribute_values('width'), n_rows.attribute_values('threshold'),
                       p_rows.attribute_values('width'), p_rows.attribute_values('threshold'),
                       tr_manager=tr_manager, wire_names=rows.wire_names_dict(),
                       n_orientations=n_rows.attribute_values('orientation'),
                       p_orientations=p_rows.attribute_values('orientation'),
                       top_layer=params.top_layer,
                       half_blk_x=True, half_blk_y=True,
                       guard_ring_nf=params.guard_ring_nf,
                       )

        # Draw the transistors
        for tx in transistors:
            ports = self.draw_mos_conn(mos_type=tx.row.channel_type.value,
                                       row_idx=rows.index_of_same_channel_type(tx.row),
                                       col_idx=tx.col,
                                       fg=tx.fg,
                                       sdir=tx.s_dir.value,
                                       ddir=tx.d_dir.value,
                                       s_net=tx.s_net,
                                       d_net=tx.d_net,
                                       gate_ext_mode=1,
                                       g_via_row=2,
                                       )
            tx.set_ports(g=ports['g'],
                         d=ports[tx.deff.value],
                         s=ports[tx.seff.value])

        # 6:  Define horizontal tracks on which connections will be made
        row_nmos_idx = rows.index_of_same_channel_type(row_nmos)
        tid_nmos_G = self.get_wire_id('nch', row_nmos_idx, 'g', wire_name='IN')
        tid_nmos_S = self.get_wire_id('nch', row_nmos_idx, 'ds', wire_name='I')
        tid_nmos_D = self.get_wire_id('nch', row_nmos_idx, 'ds', wire_name='O')

        # 7:  Perform wiring

        # NOTE: min_len_mode helps in extending track length if minimum area/length is not met by default.
        #       Need to explicitly turn this on
        warr_n_G = self.connect_to_tracks([nmos.g], tid_nmos_G, min_len_mode=0)
        warr_n_D = self.connect_to_tracks([nmos.d], tid_nmos_D, min_len_mode=0)
        warr_n_S = self.connect_to_tracks([nmos.s], tid_nmos_S, min_len_mode=0)

        # Add Pin
        self.add_pin('EN', warr_n_G, show=params.show_pins)
        self.add_pin('D', warr_n_D, show=params.show_pins)
        self.add_pin('S', warr_n_S, show=params.show_pins)

        # draw dummies
        ptap_wire_arrs, ntap_wire_arrs = self.fill_dummy()
        # export supplies
        self.add_pin('VSS', ptap_wire_arrs)
        self.add_pin('VDD', ntap_wire_arrs)

        # Define transistor properties for schematic
        tx_info = {}
        for tx in transistors:
            tx_info[tx.name] = {
                'w': tx.row.width,
                'th': tx.row.threshold,
                'fg': tx.fg
            }

        self._sch_params = dict(
            lch=params.lch,
            dum_info=self.get_sch_dummy_info(),
            tx_info=tx_info,
        )

    @property
    def sch_params(self) -> Dict[str, Any]:
        return self._sch_params


class nmos_switch(layout):
    """
    Class to be used as template in higher level layouts
    """
    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
